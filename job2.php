<?php
abstract class FarmAnimals
{
    private $id = 0; 
    private $product = 0; 
    
    function __construct() 
    { 
        $this->id = spl_object_hash($this);
    } 
    
    function SayName()
    {
        return $this->id;
    }
    
    function how()
    {
        return $this->product;
    }
    
    function clear()
    {
        $this->product = 0;
    }
    
    public function SetProduse($pr)
    {
         $this->product = $pr;
    }

    
    abstract public function Produse();
}
    
class hen extends FarmAnimals
{

      
    
    public function Produse()
    {
         $this->SetProduse( rand(0, 1));
    }

}

class cow extends FarmAnimals
{

    function Produse()
    {
         $this->SetProduse( rand(8, 12));
    }
    
}

class farm
{
    private $arrayk = array(
    "hans" => array(),
    "cows" => array(),
);

    private $now = array(
    "hans" => 0,
    "cows" => 0,
);
        private $all = array(
    "hans" => 0,
    "cows" => 0,
);

    function __construct() 
    { 
        for($i=0; $i<10; $i++) {
                $this->arrayk['hans'][] = new hen();
        }
        
        for($i=0; $i<20; $i++) {
                $cow = new cow();
                $this->arrayk['cows'][$i][0] = $cow;
                $this->arrayk['cows'][$i][1] = 0;
              // $this->arrayk['cows'][$i]= $cow;
        }
    } 
    
    public function GetProdut()
    {
        $this->now['hans'] = 0;
        $this->now['cows'] = 0;
        for($i=0; $i<10; $i++) {
                $this->arrayk['hans'][$i]->Produse();
                $this->now['hans'] = $this->now['hans'] + $this->arrayk['hans'][$i]->how();
                $this->all['hans'] = $this->all['hans']+$this->arrayk['hans'][$i]->how();
                //print $this->arrayk['hans'][$i]->how();
                //$this->arrayk['hans'][$i]->clear();
                
        }
        
        for($i=0; $i<20; $i++) {
                $this->arrayk['cows'][$i][0]->Produse();
                $this->now['cows'] = $this->now['cows']+ $this->arrayk['cows'][$i][0]->how();
                $this->all['cows'] = $this->all['cows']+$this->arrayk['cows'][$i][0]->how();
                $this->arrayk['cows'][$i][1] = $this->arrayk['cows'][$i][0]->how();
                //$this->arrayk['cows'][$i][0]->clear();
        }
    }
    
    public function All()
    {
        return $this->now['hans'] + $this->now['cows'];
    }
    
        public function Eggs()
    {
        return $this->now['hans'];
    }
    
        public function Milk()
    {
        return $this->now['cows'];
    }
    
    public function Raiting()
    {
        $ar = $this->arrayk['cows'];
        //array_multisort(array_column($ar, 0), SORT_DESC, $ar);
       foreach ($ar as $key => $row) {
            // replace 0 with the field index/key
            $dates[$key]  = $row[1];
       }
        array_multisort($dates, SORT_DESC, $ar);
        $output[0] = array_slice($ar, 0, 3);
        $output[1] = array_slice($ar, -3,3);
         return $output;
    }
}

$farm = new farm();
$farm->GetProdut();
$farm->GetProdut();
//var_dump($farm->now);
//var_dump($farm->all);
print "All production: ".$farm->All().". Eggs ".$farm->Eggs().", milk - ".$farm->Milk().".";
$mass = $farm->Raiting();
//var_dump($farm->Raiting());
$Names = "Cows with many milk:";
foreach($mass[0] as $elem)
{
    $Names = $Names.$elem[0]->SayName()." - ". $elem[1]. ". ";
}
print $Names;

$Names = "Cows without many milk:";
foreach($mass[1] as $elem)
{
    $Names = $Names.$elem[0]->SayName()." - ". $elem[1]. ". ";
}
print $Names;
    ?>